from koakuma import AUTH_TOKEN, BOORU
from koakuma.upload.sources import Parser
from koakuma.util.ugoira import get_ugoira
from koakuma.util.video import get_video

from gallery_dl.job import DataJob
from inquirer import Confirm, List, prompt, Text
from requests import codes, get, post
from tqdm.contrib import tenumerate

from io import StringIO
from json import dumps, loads
from logging import getLogger
from sys import exit

logger = getLogger(__name__)


def process(sources, answers):
    safety = answers['safety']
    tags = answers['tags'].split(',')
    items = []
    for source in sources:
        items.extend(source)

    current = 0 if answers['tags'] else int(answers['alternates'])
    relations = []
    for i, item in tenumerate(items, desc='Uploading Posts', unit='post'):
        if current:
            response = get('{}/api/post/{}'.format(BOORU, current), headers={
                'Accept': 'application/json',
                'Authorization': 'Token {}'.format(AUTH_TOKEN),
                'Content-Type': 'application/json'
            })
            json = response.json()

            if response.status_code != codes.ok:
                raise RuntimeError('tags of post "{}" could not be retrieved: {} ({})'
                                   .format(current, json['name'], json['description']))

            tags = [tag['names'][0] for tag in json['tags']]
        if item['type'] == 'url':
            response = post('{}/api/posts'.format(BOORU), json={
                'contentUrl': item['content'],
                'relations': relations + ([current] if current else []),
                'safety': safety,
                'source': item['url'],
                'tags': tags
            }, headers={
                'Accept': 'application/json',
                'Authorization': 'Token {}'.format(AUTH_TOKEN),
                'Content-Type': 'application/json'
            })
            json = response.json()

            if response.status_code != codes.ok:
                raise RuntimeError('item with content url "{}" could not be uploaded: {} ({})'
                                   .format(item['content'], json['name'], json['description']))

            if answers['relations'] and not i:
                relations.append(json['id'])
        else:
            if item['type'] == 'ugoira':
                content = get_ugoira(item['url'])
            elif item['type'] == 'bytes':
                content = item['content']
            else:
                raise NotImplemented('could not find handler for item type "{}"'.format(item['type']))

            response = post('{}/api/posts'.format(BOORU), files={
                'content': content,
                'metadata': dumps({
                    'safety': safety,
                    'source': item['url'],
                    'tags': tags
                }).encode('utf-8')
            }, headers={
                'Accept': 'application/json',
                'Authorization': 'Token {}'.format(AUTH_TOKEN)
            })
            json = response.json()

            if response.status_code != codes.ok:
                raise RuntimeError('item with url "{}" could not be uploaded: {} ({})'.format(item['url'], json['name'],
                                                                                              json['description']))
        if current:
            current += 1


def upload(args):
    sources = []
    for url in args.source:
        if args.extract:
            DataJob(url).run()
        else:
            fp = StringIO()
            DataJob(url, file=fp).run()
            data = loads(fp.getvalue())
            parent = 0
            uploads = []
            if not len(data):
                uploads.append({
                    'content': Parser(url).run(),
                    'type': 'bytes',
                    'url': url
                })
            elif len(data) == 1:
                uploads.append({
                    'content': uploads[0][0]['content'],
                    'type': 'url',
                    'url': url
                })
            else:
                for image in data:
                    if image[0] == 2:
                        parent += 1
                    else:
                        uploads.append({
                            'content': image[1],
                            'type': 'url',
                            'url': url
                        })
                    if parent > 1:
                        break
            sources.append(uploads)

    if args.extract:
        return

    questions = [
        Text('tags', message='Tags for Post(s)'),
        Text('alternates', 'Start of Alternate Post(s)', ignore=lambda current: current['tags']),
        List('safety', message='Post Safety', choices=['safe', 'sketchy', 'unsafe'], carousel=True),
        Confirm('relations', message='Set Post Relation?', ignore=len(sources) != 1 or len(sources[0]) == 1)
    ]
    try:
        answers = prompt(questions, raise_keyboard_interrupt=True)
    except KeyboardInterrupt:
        return

    try:
        process(sources, answers)
    except RuntimeError as e:
        logger.error('upload was not processed successfully: {}'.format(str(e)))
        exit(1)
