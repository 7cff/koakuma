from gallery_dl.extractor.twitter import TwitterTweetExtractor
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import NoSuchElementException

tweet_cleanup = \
'''
let list = document.querySelectorAll('article > div > div > div > div:nth-child(3) > div:nth-child(n+4)');
list.forEach(node => node.parentNode.removeChild(node));
document.querySelector('article div:nth-child(2) > div > div > div > div > div > svg').remove();
'''

class Source:
    test = TwitterTweetExtractor.pattern
    __name__ = 'TwitterTextSource'

    def __init__(self, url):
        self.url = url

    def run(self):
        options = Options()
        options.headless = True
        with Firefox(options=options) as driver:
            driver.implicitly_wait(10)
            driver.get(self.url)
            driver.find_element_by_css_selector('article > div > div > div > div:nth-child(3) > div:nth-child(n+4)')
            # ensure we select the first tweet before we narrow down the svg
            driver.find_element_by_css_selector('article div:nth-child(2) > div > div > div > div > div').find_element_by_css_selector('svg')
            driver.execute_script(tweet_cleanup)
            return driver.find_element_by_css_selector('article').screenshot_as_png
