from importlib import import_module
from os import listdir
from os.path import dirname, isfile, join
from re import match


class Parser:
    def __init__(self, url):
        self.__loader = self.__load_sources()
        self.__sources = []
        self.url = url

    def __load_sources(self):
        sources = dirname(__file__)
        for source in (file for file in listdir(sources) if isfile(join(sources, file)) and '__init__' not in file):
            name, _ = source.rsplit('.', 1)
            module = import_module('koakuma.upload.sources.{}'.format(name))
            cls = getattr(module, 'Source')
            self.__sources.append(cls)
            yield cls

    def run(self):
        for source in self.__sources:
            if test(source, self.url):
                return source(self.url)

        while True:
            try:
                source = next(self.__loader)
                if test(source, self.url):
                    return source(self.url).run()
            except StopIteration:
                raise NotImplemented('url "{}" doesn\'t have an associated source'.format(self.url))


def test(source, url):
    return bool(match(source.test, url))
