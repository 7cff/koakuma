from koakuma import AUTH_TOKEN, BOORU

from inquirer import Editor, prompt, Text
from requests import codes, post, put


def add_artist(args):
    questions = [
        Text('implications', message='Implications'),
        Editor('description', 'Description')
    ]
    try:
        answers = prompt(questions, raise_keyboard_interrupt=True)
    except KeyboardInterrupt:
        return
    implications = answers['implications'].split(',')

    response = post('{}/api/tags'.format(BOORU), json={
        'category': 'artist',
        'description': answers['description'],
        'names': [args.artist]
    }, headers={
        'Accept': 'application/json',
        'Authorization': 'Token {}'.format(AUTH_TOKEN),
        'Content-Type': 'application/json'
    })
    json = response.json()

    if response.status_code != codes.ok:
        raise RuntimeError('tag "{}" could not be created: {} ({})'.format(args.artist, json['name'],
                                                                           json['description']))

    # POST /tags doesn't work on category and description fields for some reason, so we need to resort to an extra PUT
    # call
    response = put('{}/api/tag/{}'.format(BOORU, args.artist), json={
        'category': 'artist',
        'description': answers['description'],
        'version': json['version']
    }, headers={
        'Accept': 'application/json',
        'Authorization': 'Token {}'.format(AUTH_TOKEN),
        'Content-Type': 'application/json'
    })
    json = response.json()

    if response.status_code != codes.ok:
        raise RuntimeError('tag "{}" could not be updated: {} ({})'.format(args.artist, json['name'],
                                                                           json['description']))
